from indent_tree import load_string
def test_load_string_stackoverflow1():
    indented_text = """
    apple
        colours
            red
            yellow
            green
        type
            granny smith
        price
            0.10
    """
    d = load_string(indented_text, single_child_as_value=True)
    assert "apple" in d.keys()
    apple = d['apple']
    assert len(apple.keys()) == 3
    assert "colours" in apple.keys()
    assert "type" in apple.keys()
    assert "price" in apple.keys()
    assert apple['price'] == '0.10'
    assert apple['type'] == "granny smith"
    assert "red" in apple['colours']
    assert "yellow" in apple['colours']
    assert "green" in apple['colours']

def test_load_string_with_colons():
    indented_text = """
    a:
        b:      c
        d:      e
    a:
        b:      c2
        d:      e2
        d:      wrench
    """
    d = load_string(indented_text, interpret_colons=True)
    assert len(d) == 2
    assert d[0]["a"]["b"] == 'c'
    assert d[0]["a"]["d"] == 'e'
    assert d[1]["a"][0]['b'] == 'c2'
    assert d[1]["a"][1]["d"] == 'e2'
    assert d[1]["a"][2]["d"] == 'wrench'


