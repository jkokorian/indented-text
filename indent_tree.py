from collections import ChainMap

class Node:
    def __init__(self, indented_line, *, interpret_colons=False, single_child_as_value=False):
        self.children = []
        self.level = len(indented_line) - len(indented_line.lstrip())
        self.text = indented_line.strip()

        self.interpret_colons = interpret_colons
        self.single_child_as_value = single_child_as_value
        
    def _add_children(self, nodes):
        childlevel = nodes[0].level
        while nodes:
            node = nodes.pop(0)
            if node.level == childlevel:
                self.children.append(node)
            elif node.level > childlevel:
                nodes.insert(0,node)
                self.children[-1]._add_children(nodes)
            elif node.level <= self.level:
                nodes.insert(0,node)
                return
    
    def all_child_keys_unique(self):
        return len(self.children) == len(set(n.key() for n in self.children))

    def key(self):
        return self.text.split(':',1)[0] if self.interpret_colons else self.text

    def has_value(self):
        return any(self.children) or self.interpret_colons and ":" in self.text

    def value(self):
        if self.children and self.all_child_keys_unique() and all(n.has_value() for n in self.children):
            return dict(ChainMap(*[n.as_dict() for n in self.children]))
        elif self.single_child_as_value and len(self.children) == 1:
            return self.children[0].key()
        elif self.children:
            return [node.as_dict() for node in self.children]
        elif self.interpret_colons and ":" in self.text:
            return self.text.split(':',1)[1].strip()
        else:
            return None

    def as_dict(self) -> dict:
        key = self.key()
        value = self.value()
        return {key:value} if value is not None else key

    def __repr__(self):
        return self.text    
    
def load_string(indented_text, **kwargs):
        root = Node('root', **kwargs)
        root._add_children([Node(line, **kwargs) for line in indented_text.splitlines() if line.strip()])
        return root.as_dict()['root']